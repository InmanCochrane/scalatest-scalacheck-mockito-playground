import org.mockito.{Mockito, MockitoSugar}
import org.scalacheck.{Arbitrary, Gen}
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

class MyTest extends AnyFunSuite with Matchers with MockitoSugar {

  test("works with mockito-scala") {
    val foo = mock[FooClass]
    val stubResult = "stubbed result"
    when(foo.barMethod).thenReturn(stubResult)

    foo.barMethod shouldBe stubResult
  }

  test("works with mockito-scala mocking an object") {
    // Note: object must be defined in runtime code
    withObjectMocked[FooObject.type] {
      val stubResult = "stubbed object result"
      when(FooObject.barMethod).thenReturn(stubResult)
      FooObject.barMethod shouldBe stubResult
    }
  }

  test("works with scalacheck generators") {
    val foo: Gen[FooClass] = new FooClass
    ScalaCheckPropertyChecks.forAll(foo) { f =>
      f.barMethod shouldBe "default method result"
    }
  }

  test("works with scalacheck generators and mockito verification") {
    ScalaCheckPropertyChecks.forAll(Arbitrary.arbInt.arbitrary) { n =>
      val foo = mock[FooClass]
      Mockito.when(foo.bazMethod(n)).thenReturn(-n)
      foo.bazMethod(n)
      Mockito.verify(foo, times(1)).bazMethod(n)
    }
  }

}
