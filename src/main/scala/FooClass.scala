class FooClass {
  def barMethod = "default method result"
  def bazMethod(n: Int): Int = n
}
